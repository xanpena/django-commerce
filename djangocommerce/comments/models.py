from django.db import models
from django.conf import settings

# Create your models here.
class Comment(models.Model):
    content = models.TextField()
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete = models.CASCADE,
        related_name='comments'
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    product = models.ForeignKey(
        'products.Product',
        related_name='comments',
        on_delete=models.CASCADE
    )


    def __str__(self):
        return self.content[:20]