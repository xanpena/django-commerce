from django.test import TestCase, Client

# Create your tests here.
class HomeTest(TestCase):

    def setup(self):
        self.client = Client()
        self.response = client.get("/")

    def test_home(self):
        self.assertTrue(1==1)
        self.assertFalse(1!=1)
        self.assertEqual(1, 1)
        self.assertIn(1, [1, 2, 3])

    def test_render(self):
        self.assertEquals(self.response.status_code, 200)
        #print(response.content)
    
    def test_context(self):
        self.assertIn('products', self.response.context)
    
    def test_products(self):
        products = self.response.context['products']
        self.assertEquals(len(products), 0)
